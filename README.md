# SiTukin (Sistem Informasi Tunjangan Kinerja)
Merupakan sebuah project sederhana yang mengambil obyek berupa Tunjangan Kinerja di __RRI BANJARMASIN__ , bertujuan untuk menghitung jumlah tunjangan yang akan diperoleh pegawai sesuai dengan kinerja yang telah dicapai oleh pegawai yang bersangkutan.

![RRI Banjarmasin](./assets/images/logo.jpeg)

## Deskripsi
Aplikasi ini sangatlah sederhana, adapun engine yang digunakan dalam pemrograman ini adalah menggunakan bahasa __PHP Murni Versi 7.x.x__ yang telah saya racik sedemikian rupa sehingga saya dapat sebut sebagai __Framework PHP__ namun dengan sangat sederhana, dengan harapan agar dapat dengan mudah untuk dipahami oleh orang yang baru ingin belajar PHP.

Coding yang saya gunakan tidaklah murni buatan saya akan tetapi di beberapa bagian menggunakan Library PHP yang telah dikembangkan oleh orang-orang yang memang kompeten dibidangnya, adapun credit untuk perihal tersebut dapat dilihat pada rincian berikut.

## Requirement
1. PHP 7 > (recomended)
2. MySQL (for database)

## Library PHP
1. PHP Session (https://github.com/Josantonius/PHP-Session)
2. PHP MySQLi Database Class (https://github.com/ThingEngineer/PHP-MySQLi-Database-Class)
3. Twig 2 (https://github.com/twigphp/Twig) (masih tahap pengembangan dan belum dapat di aplikasikan dalam framework ini)

## Template
1. AdminLTE2 (https://github.com/almasaeed2010/AdminLTE) (bisa costum sendiri di folder assets)
2. Simple HTTP Error Page Generator (https://github.com/AndiDittrich/HttpErrorPages).

Akhir kata saya ucapkan terimakasih banyak kepada semua pihak yang telah membagikan Source Code secara gratis. silahkan klik tautan yang tertera untuk melihat repository milik mereka masing-masing ;)

## Support Me
For Donate : ***BRI (0239 01 023305 50 9) A.n Nasrullah Siddik*** Thanks :).
## Contact 
1. Telegram :[@as_shiddiq](http://telegram.me/@as_shiddiq)
2. Facebook : [as.shiddiq](http://fb.com/as.shiddiq)
3. Email : n.shiddiq@gmail.com

# PREVIEW
## Form Login
![SITUKIN](./assets/screenshoot/login.png)

## Dashboard
![SITUKIN](./assets/screenshoot/home.png)
## Kinerja Bulanan
![SITUKIN](./assets/screenshoot/kinerjabulanan.png)

![SITUKIN](./assets/screenshoot/kinerjabulanan2.png)

![SITUKIN](./assets/screenshoot/kinerjabulanan3.png)
## Tunjangan Kinerja
![SITUKIN](./assets/screenshoot/tukin.png)

![SITUKIN](./assets/screenshoot/tukin2.png)
