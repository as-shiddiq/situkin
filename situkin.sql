-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 07, 2017 at 01:43 PM
-- Server version: 10.1.26-MariaDB
-- PHP Version: 7.1.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `situkin`
--

-- --------------------------------------------------------

--
-- Table structure for table `bank`
--

CREATE TABLE `bank` (
  `id_bank` int(11) NOT NULL,
  `nama_bank` varchar(100) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bank`
--

INSERT INTO `bank` (`id_bank`, `nama_bank`) VALUES
(1, 'PT.BANK RAKYAT INDONESIA (Persero) Tbk. KC BANJARMASIN'),
(2, 'BRI Unit A. Yani');

-- --------------------------------------------------------

--
-- Table structure for table `jabatan`
--

CREATE TABLE `jabatan` (
  `id_jabatan` int(11) NOT NULL,
  `id_kelas_jabatan` int(11) NOT NULL,
  `id_unit_kerja` int(11) NOT NULL,
  `nama_jabatan` varchar(30) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jabatan`
--

INSERT INTO `jabatan` (`id_jabatan`, `id_kelas_jabatan`, `id_unit_kerja`, `nama_jabatan`) VALUES
(1, 5, 1, 'Ajun Andalan Siaran'),
(2, 5, 0, 'Kasi Pro 1');

-- --------------------------------------------------------

--
-- Table structure for table `kehadiran`
--

CREATE TABLE `kehadiran` (
  `id_kehadiran` int(11) NOT NULL,
  `id_pegawai` int(11) NOT NULL,
  `tanggal_kehadiran` date NOT NULL,
  `tl1` enum('Y','T') NOT NULL DEFAULT 'T',
  `tl2` enum('Y','T') NOT NULL DEFAULT 'T',
  `psw1` enum('Y','T') NOT NULL DEFAULT 'T',
  `psw2` enum('Y','T') NOT NULL DEFAULT 'T',
  `tm1` enum('Y','T') NOT NULL DEFAULT 'T',
  `tm2` enum('Y','T') NOT NULL DEFAULT 'T',
  `izin` enum('Y','T') NOT NULL DEFAULT 'T'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kehadiran`
--

INSERT INTO `kehadiran` (`id_kehadiran`, `id_pegawai`, `tanggal_kehadiran`, `tl1`, `tl2`, `psw1`, `psw2`, `tm1`, `tm2`, `izin`) VALUES
(3, 1, '2017-11-14', 'Y', 'T', 'T', 'T', 'T', 'T', 'T'),
(2, 1, '2017-11-22', 'Y', 'Y', 'Y', 'T', 'T', 'T', 'T');

-- --------------------------------------------------------

--
-- Table structure for table `kelas_jabatan`
--

CREATE TABLE `kelas_jabatan` (
  `id_kelas_jabatan` int(11) NOT NULL,
  `kelas_jabatan` int(2) NOT NULL,
  `besar_tunjangan` float(15,2) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kelas_jabatan`
--

INSERT INTO `kelas_jabatan` (`id_kelas_jabatan`, `kelas_jabatan`, `besar_tunjangan`) VALUES
(5, 2, 100000.00);

-- --------------------------------------------------------

--
-- Table structure for table `kinerja_bulanan`
--

CREATE TABLE `kinerja_bulanan` (
  `id_kinerja_bulanan` int(11) NOT NULL,
  `id_pegawai` int(11) NOT NULL,
  `total_pokok` float(15,2) NOT NULL,
  `bobot_pokok` float(15,2) NOT NULL,
  `total_tambahan` float(15,2) NOT NULL,
  `bobot_tambahan` float(15,2) NOT NULL,
  `total_prestasi` float(15,2) NOT NULL,
  `penilaian` varchar(1) NOT NULL,
  `pengurangan_tukin` float(15,2) NOT NULL,
  `predikat` varchar(20) NOT NULL,
  `tanggal_penilaian` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kinerja_bulanan`
--

INSERT INTO `kinerja_bulanan` (`id_kinerja_bulanan`, `id_pegawai`, `total_pokok`, `bobot_pokok`, `total_tambahan`, `bobot_tambahan`, `total_prestasi`, `penilaian`, `pengurangan_tukin`, `predikat`, `tanggal_penilaian`) VALUES
(1, 1, 0.00, 0.00, 0.00, 0.00, 0.00, 'E', 20.00, 'E', '2017-12-07');

-- --------------------------------------------------------

--
-- Table structure for table `kinerja_bulanan_sub`
--

CREATE TABLE `kinerja_bulanan_sub` (
  `id_kinerja_bulanan_sub` int(11) NOT NULL,
  `id_kinerja_bulanan` int(11) NOT NULL,
  `id_target` int(11) NOT NULL,
  `realisasi` float(15,2) NOT NULL,
  `kinerja` float(15,2) NOT NULL,
  `prosentase` float(15,2) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pegawai`
--

CREATE TABLE `pegawai` (
  `id_pegawai` int(11) NOT NULL,
  `id_bank` int(11) NOT NULL,
  `id_jabatan` int(11) NOT NULL,
  `nip` varchar(25) NOT NULL,
  `nama_pegawai` varchar(30) NOT NULL,
  `pangkat_gol` varchar(50) NOT NULL,
  `tmt` date NOT NULL,
  `status_pegawai` varchar(10) NOT NULL,
  `status_perkawinan` enum('TK','K') NOT NULL,
  `jumlah_anak` int(2) NOT NULL,
  `npwp` varchar(25) NOT NULL,
  `no_rek` varchar(30) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pegawai`
--

INSERT INTO `pegawai` (`id_pegawai`, `id_bank`, `id_jabatan`, `nip`, `nama_pegawai`, `pangkat_gol`, `tmt`, `status_pegawai`, `status_perkawinan`, `jumlah_anak`, `npwp`, `no_rek`) VALUES
(1, 1, 1, '196107121982032001', 'Akhmad Surya Permana', 'Penata Muda TK.I/IIIb', '2015-10-05', 'PNS', 'TK', 0, '696196526731000', '000301055074507'),
(2, 1, 2, '197506221997032004', 'NORDIANA,S.Sos', '', '0000-00-00', 'PNS', 'TK', 0, '696196500731000', '000301055016509');

-- --------------------------------------------------------

--
-- Table structure for table `pengguna`
--

CREATE TABLE `pengguna` (
  `id_pengguna` int(11) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(20) NOT NULL,
  `nama_lengkap` varchar(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pengguna`
--

INSERT INTO `pengguna` (`id_pengguna`, `username`, `password`, `nama_lengkap`) VALUES
(1, 'admin', 'admin', '');

-- --------------------------------------------------------

--
-- Table structure for table `target`
--

CREATE TABLE `target` (
  `id_target` int(11) NOT NULL,
  `id_jabatan` int(11) NOT NULL,
  `urutan` int(11) NOT NULL,
  `komponen_capaian` varchar(100) NOT NULL,
  `target` float(15,2) NOT NULL,
  `jenis` enum('pokok','tambahan') NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `target`
--

INSERT INTO `target` (`id_target`, `id_jabatan`, `urutan`, `komponen_capaian`, `target`, `jenis`) VALUES
(1, 1, 1, 'Mengumpulkan & menyusun data / Informasi', 15.00, 'pokok'),
(2, 1, 2, 'Mengumpulkan & menyusun bahan Audio kelengkapan liputan', 15.00, 'pokok'),
(3, 1, 1, 'Upacara Bendera', 1.00, 'tambahan'),
(4, 1, 3, 'Menyiapkan naskah laporan pandangan mata', 2.00, 'pokok'),
(5, 1, 4, 'Melaksanakan laporan pandangan matabaik langsung maupun rekaman', 2.00, 'pokok'),
(6, 1, 5, 'Melaksanakan Cheking audio/vidio hasil liputan', 15.00, 'pokok'),
(7, 1, 6, 'Mengikuti & mengawasi proses editing untuk bahan siaran', 15.00, 'pokok'),
(8, 1, 7, 'Melaksanakan tugas peliputan', 15.00, 'pokok'),
(9, 1, 8, 'Menentukan materi berita', 15.00, 'pokok'),
(10, 1, 9, 'Memeriksa naskah yang akan dibacakan', 15.00, 'pokok'),
(11, 1, 9, 'Mengoreksi naskah berita', 15.00, 'pokok'),
(12, 1, 10, 'Membaca naskah berita', 15.00, 'pokok'),
(13, 1, 11, 'Menyajikan acara (Presenter)', 4.00, 'pokok'),
(14, 1, 12, 'Melaksnakan pembahasan redaksi', 4.00, 'pokok'),
(15, 1, 13, 'Menyusun urutan berita', 5.00, 'pokok'),
(16, 1, 14, 'Mengikuti Pertemuan produksi', 4.00, 'pokok'),
(17, 1, 16, 'Menulis naskah berita', 15.00, 'pokok'),
(18, 1, 2, 'senam pagi', 4.00, 'tambahan'),
(19, 1, 3, 'Rapat pleno karyawan', 1.00, 'tambahan'),
(20, 1, 4, 'rapat all struktural', 0.00, 'tambahan'),
(21, 1, 5, 'rapat kabid/kabag', 0.00, 'tambahan');

-- --------------------------------------------------------

--
-- Table structure for table `unit_kerja`
--

CREATE TABLE `unit_kerja` (
  `id_unit_kerja` int(11) NOT NULL,
  `unit_kerja` varchar(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `unit_kerja`
--

INSERT INTO `unit_kerja` (`id_unit_kerja`, `unit_kerja`) VALUES
(1, 'BIDANG PEMBERITAAN');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bank`
--
ALTER TABLE `bank`
  ADD PRIMARY KEY (`id_bank`);

--
-- Indexes for table `jabatan`
--
ALTER TABLE `jabatan`
  ADD PRIMARY KEY (`id_jabatan`);

--
-- Indexes for table `kehadiran`
--
ALTER TABLE `kehadiran`
  ADD PRIMARY KEY (`id_kehadiran`);

--
-- Indexes for table `kelas_jabatan`
--
ALTER TABLE `kelas_jabatan`
  ADD PRIMARY KEY (`id_kelas_jabatan`);

--
-- Indexes for table `kinerja_bulanan`
--
ALTER TABLE `kinerja_bulanan`
  ADD PRIMARY KEY (`id_kinerja_bulanan`);

--
-- Indexes for table `kinerja_bulanan_sub`
--
ALTER TABLE `kinerja_bulanan_sub`
  ADD PRIMARY KEY (`id_kinerja_bulanan_sub`);

--
-- Indexes for table `pegawai`
--
ALTER TABLE `pegawai`
  ADD PRIMARY KEY (`id_pegawai`);

--
-- Indexes for table `pengguna`
--
ALTER TABLE `pengguna`
  ADD PRIMARY KEY (`id_pengguna`);

--
-- Indexes for table `target`
--
ALTER TABLE `target`
  ADD PRIMARY KEY (`id_target`);

--
-- Indexes for table `unit_kerja`
--
ALTER TABLE `unit_kerja`
  ADD PRIMARY KEY (`id_unit_kerja`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bank`
--
ALTER TABLE `bank`
  MODIFY `id_bank` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `jabatan`
--
ALTER TABLE `jabatan`
  MODIFY `id_jabatan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `kehadiran`
--
ALTER TABLE `kehadiran`
  MODIFY `id_kehadiran` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `kelas_jabatan`
--
ALTER TABLE `kelas_jabatan`
  MODIFY `id_kelas_jabatan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `kinerja_bulanan`
--
ALTER TABLE `kinerja_bulanan`
  MODIFY `id_kinerja_bulanan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `kinerja_bulanan_sub`
--
ALTER TABLE `kinerja_bulanan_sub`
  MODIFY `id_kinerja_bulanan_sub` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pegawai`
--
ALTER TABLE `pegawai`
  MODIFY `id_pegawai` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `pengguna`
--
ALTER TABLE `pengguna`
  MODIFY `id_pengguna` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `target`
--
ALTER TABLE `target`
  MODIFY `id_target` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `unit_kerja`
--
ALTER TABLE `unit_kerja`
  MODIFY `id_unit_kerja` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
