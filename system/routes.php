<?php
  // auth
  Auth(['home','pegawai','kelasjabatan','jabatan','bank','pegawai','kehadiran'],'login');

  // route
  $resource['home']=array('home',true);
  $resource['kelasjabatan']=array('kelasjabatan',true);
  $resource['jabatan']=array('jabatan',true);
  $resource['bank']=array('bank',true);
  $resource['pegawai']=array('pegawai',true);
  $resource['kehadiran']=array('kehadiran',true);
  $resource['tukin']=array('tukin',true);
  $resource['target']=array('target',true);
  $resource['kinerjabulanan']=array('kinerjabulanan',true);
  $resource['unitkerja']=array('unitkerja',true);

  $resource['cetak.tukin']=array('cetak/tukin',false);

  $resource['login']=array('login',false);
  $resource['logout']=array('logout',false);

 ?>
