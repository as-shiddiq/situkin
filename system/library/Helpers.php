<?php
	function templates(){
		$template=$GLOBALS['template'];
		return assets($template.'/');
	}
	function assets($a=''){
		return 'assets/'.$a;
	}
	function redirect($a){
		header('location:?view='.$a);
	}
	function url($url){
		return '?view='.$url;
	}
	function request_all($fillable){
		foreach ($fillable as $key => $value) {
      if(isset($_POST[$value])){
        $data[$value]=$_POST[$value];
      }
    }
		return $data;
	}
	function Auth($check,$to){
		$view='home';
		if(isset($_GET['view'])){
			$view=$_GET['view'];
		}
		if(in_array($view,$check)){
			if(Session::get('logged')!='logged'){
				redirect($to);
			}
		}
	}

?>
