<?php
$setview=true;
$_error=false;
$fileerror='view/errors/404.php';
if(isset($_GET['view'])){
	$showview=$_GET['view'];
	if(array_key_exists($showview,$resource)){
		$file='view/'.$resource[$showview][0].'.php';
		$setview=$resource[$showview][1];
		if(!file_exists($file)){
			$_error=true;
			$file=$fileerror;
		}
	}
	else{
		$file='view/errors/500.php';
		$setview=false;
	}
}
else{
		$file='view/home.php';
}
if($setview==true AND $_error==false){
	ob_start();
		include $file;
	$view = ob_get_contents();
	ob_end_clean();
	include('view/layouts/Html.php');
}
else{
	include $file;
}

?>
