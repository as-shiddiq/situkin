<?php
$title='Login';
if(count($_POST)>0){
  $db->where('username',$_POST['username']);
  $db->where('password',$_POST['password']);
  $r=$db->ObjectBuilder()->getOne('pengguna');
  if($db->count){
    Session::set('logged','logged');
    Session::set('id_pengguna',$r->id_pengguna);
    Session::set('nama_lengkap',$r->nama_lengkap);
    Session::set('info','<b>'.$r->nama_lengkap.'</b> Anda telah sukses masuk ke dalam sistem');
    redirect('home');
  }
  else{
    Session::set('info','Nama Pengguna tau kata sandi salah');
    redirect('login');
  }
}
else{
 ?>
<!DOCTYPE html>
<html>
  <head>
    <?php include('layouts/head.php');?>
    <style media="screen">
      body{
        background: white !important
      }
    </style>
  </head>
    <body class="hold-transition login-page">
      <div class="login-box">
      <div class="login-logo">
        <a><b>SI</b>TUKIN</a>
      </div>
      <!-- /.login-logo -->
      <div class="login-box-body bg-red">
        <?php
          if(Session::get('info')){
            ?>
            <div class="alert alert-danger" role="alert">
            <?=Session::get('info');?>
            </div>
          <?php
            Session::destroy('info');
          }
         ?>
         <div class="clearfix text-center">
           <img src="<?=assets()?>images/logo.jpeg" alt="rri banjarmasin">
         </div>
         <hr>
        <p class="login-box-msg">Sign in to start your session</p>

        <form action="<?=url('login')?>" method="post">
          <div class="form-group has-feedback">
            <input type="text" class="form-control" placeholder="username" name="username">
            <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
          </div>
          <div class="form-group has-feedback">
            <input type="password" class="form-control" placeholder="Password" name="password">
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
          </div>
          <button type="submit" class="btn btn-info btn-block btn-flat">Sign In</button>
            <!-- /.col -->
        </form>
        <div class="row">

        </div>

      </div>
      <!-- /.login-box-body -->
<!-- /.login-box -->

    <!-- Javascript files-->
    <?php include('layouts/javascript.php');?>
  </body>
</html>
<?php } ?>
