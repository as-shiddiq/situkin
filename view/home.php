<?php $title="Menu Utama"; ?>
<section class="content-header">
  <h1>
    Dashboard
    <small>Control panel</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Dashboard</li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <?php
  if(Session::get('info')){
    echo info_success(Session::get('info'));
    Session::destroy('info');
  }
  ?>
        <div class="col-md-4">
          <div class="row">
            <div class="col-md-12  bg-purple">
              <h3>Welcome</h3>
              <p>
                Selamat datang di halaman utama SITUKIN RRI Banjarmasin. silahkan gunakan sistem sebagaimana mestinya.
              </p>
            </div>
            <div class="col-md-12 bg-red">
                <h3>Tentang</h3>
                <p>
                  Situkin (Sistem Informasi Tunjangan Kinerja) merupakan sebuah sistem yang dibangun untuk keperluan menghitung berasa besar jumlah tunjangan yang diterima pegawai setiap bulannya, berdasarkan kriteria-kriteria yang telah ditentukan
                </p>
            </div>
            <div class="col-md-12 bg-yellow">
                <h3>RIwayat</h3>
                <p>
                  Situkin Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad
                </p>
            </div>
          </div>

      </div>
      <div class="col-md-8">
        <div id="myCarousel" class="carousel slide" data-ride="carousel">
            <!-- Indicators -->
          <ol class="carousel-indicators">
            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
            <li data-target="#myCarousel" data-slide-to="1"></li>
            <li data-target="#myCarousel" data-slide-to="2"></li>
          </ol>

          <!-- Wrapper for slides -->
          <div class="carousel-inner">
            <div class="item active">
              <img src="<?=assets()?>images/slide/1.jpg" alt="slide3">
              <div class="carousel-caption">
               <h3>SLIDE 1</h3>
               <p>We love the Big Apple!</p>
             </div>
            </div>

            <div class="item">
              <img src="<?=assets()?>images/slide/2.jpg" alt="slide2">
              <div class="carousel-caption">
               <h3>SLIDE 2</h3>
               <p>We love the Big Apple!</p>
             </div>
            </div>

            <div class="item">
              <img src="<?=assets()?>images/slide/3.jpg" alt="slide 1">
              <div class="carousel-caption">
               <h3>SLIDE 3</h3>
               <p>We love the Big Apple!</p>
             </div>
            </div>
          </div>

          <!-- Left and right controls -->
          <a class="left carousel-control" href="#myCarousel" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left"></span>
            <span class="sr-only">Previous</span>
          </a>
          <a class="right carousel-control" href="#myCarousel" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right"></span>
            <span class="sr-only">Next</span>
          </a>
        </div>
      </div>
      <div class="clearfix">

      </div>
</section>
