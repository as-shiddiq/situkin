<?php
$title="Tunjangan ";
$url="tukin";
$bulan=($_GET['bulan']??date('m'));
$tahun=($_GET['tahun']??date('Y'));
header("Content-type: application/vnd-ms-excel");
header("Content-Disposition: attachment; filename=tukin".bulan_huruf($bulan).$tahun.".xls");
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title></title>
    <style media="screen">
      .bold{
        font-weight: bold;
      }
    </style>
  </head>
  <body>

  <table class="bold">
    <tr>
      <td colspan="4">REKAPITULASI TUNJANGAN KINERJA PEGAWAI</td>
    </tr>
    <tr>
      <td>UNIT KERJA</td>
      <td colspan="2">: RRI BANJARMASIN</td>
    </tr>
    <tr>
      <td>BULAN</td>
      <td colspan="2">: <?=bulan_huruf($bulan)?> <?=$tahun?>&nbsp;</td>
    </tr>
  </table>
  <br>
  <table class="table table-bordered table-me" border="1">
    <thead class="bg-abu">
      <tr>
        <th width="10px" rowspan="4">No</th>
        <th rowspan="4">Nama</th>
        <th rowspan="4">NIP/NRP</th>
        <th rowspan="4">Status Pegawai</th>
        <th rowspan="4" colspan="2">Status Perkawinan/Jml Anak</th>
        <th rowspan="4">NPWP</th>
        <th rowspan="4">Nama BANK</th>
        <th rowspan="4">No. Rekening</th>
        <th rowspan="4">Jabatan</th>
        <th rowspan="4">Penghasilan Bulan Terakhir</th>
        <th rowspan="4">Kelas Jabatan</th>
        <th rowspan="4">Besaran Tunjangan</th>
        <th colspan="27">FAKTOR PENGURANGAN TUNJANGAN</th>
        <th rowspan="4">Jumlah Penguranagan Tunjangan Kinerja (Rp)</th>
        <th rowspan="4">Tunjangan Kinerja yang diterima</th>
      </tr>
      <tr>
        <th colspan="3" rowspan="2">Nilai Prestasi Kerja Pegawai Tahun Lalu</th>
        <th colspan="3" rowspan="2">Nilai Prestasi Kerja Pegawai Bulanan</th>
        <th colspan="21">Kehadiran</th>
      </tr>
      <tr>
        <th colspan="3">Terlambat 1 Menit s.d. 60 Menit (TL1)</th>
        <th colspan="3">Terlambat > 60 menit dan tidak mengganti waktu keterlambatan (TL2)</th>
        <th colspan="3">Pulang Kerja 1 menit s.d. 60 menit sebelum waktu pulang (PSW1)</th>
        <th colspan="3">Pulang Kerja lebih dari 60 menit sebelum waktu pulang (PSW2)</th>
        <th colspan="3">Tidak Masuk Kerja tanpa alasan sah</th>
        <th colspan="3">Tidak Masuk Kerja dengan seijin atasan</th>
        <th colspan="3">Sakit, cuti besar, cuti bersalin, dan/atau cuti karena alasan penting</th>
      </tr>
      <tr>
        <th>Nilai</th>
        <th>% Kurang</th>
        <th>Jumlah (Rp)</th>
        <th>Nilai</th>
        <th>% Kurang</th>
        <th>Jumlah (Rp)</th>
        <th>% Kurang</th>
        <th>Frekuensi</th>
        <th>Jumlah (Rp)</th>
        <th>% Kurang</th>
        <th>Frekuensi</th>
        <th>Jumlah (Rp)</th>
        <th>% Kurang</th>
        <th>Frekuensi</th>
        <th>Jumlah (Rp)</th>
        <th>% Kurang</th>
        <th>Frekuensi</th>
        <th>Jumlah (Rp)</th>
        <th>% Kurang</th>
        <th>Frekuensi</th>
        <th>Jumlah (Rp)</th>
        <th>% Kurang</th>
        <th>Frekuensi</th>
        <th>Jumlah (Rp)</th>
        <th>% Kurang</th>
        <th>Frekuensi</th>
        <th>Jumlah (Rp)</th>
      </tr>
    </thead>
    <tbody>
      <?php
        $i=0;

        $db->join("jabatan b", "a.id_jabatan=b.id_jabatan", "LEFT");
        $db->join("bank c", "a.id_bank=c.id_bank", "LEFT");
        $db->join("kelas_jabatan d", "b.id_kelas_jabatan=d.id_kelas_jabatan", "LEFT");
        $data=$db->ObjectBuilder()->get('pegawai a');
        foreach ($data as $row) {
          $besar_tunjangan=$row->besar_tunjangan;

          $a1='Baik';
          $b1='0';
          $c1='0';

          //ambil tukin
          $db->where('id_pegawai ='.$row->id_pegawai);
          $db->where('DATE_FORMAT(tanggal_penilaian,"%Y %m") ="'.$tahun.' '.$bulan.'"');
          $getkinerjabulanan=$db->ObjectBuilder()->getOne('kinerja_bulanan');
          if(count($getkinerjabulanan)>0){
            $a2=$getkinerjabulanan->penilaian;
            $b2=$getkinerjabulanan->pengurangan_tukin;
            $c2=($besar_tunjangan*$b2)/100;
            $kinerjabulanan='<td>'.$a2.'</td>
                            <td class="text-center">'.$b2.'</td>
                            <td>'.uangindonesia($c2).'</td>';
            $background='';
          }
          else{
            $b2=20;
            $c2=($besar_tunjangan*$b2)/100;
            $kinerjabulanan='<td colspan="3" bgcolor="red"><i>belum diatur</i></td>';
            $background='bgcolor="red"';
          }
          $get=$db->ObjectBuilder()
                    ->where('DATE_FORMAT(tanggal_kehadiran, "%Y %m")="'.$tahun.' '.$bulan.'"')
                    ->where('id_pegawai ='.$row->id_pegawai)
                    ->get('kehadiran',null,'IFNULL(SUM(IF(tl1="Y",1,0)),0) as tl1,IFNULL(SUM(IF(tl2="Y",1,0)),0) as tl2,IFNULL(SUM(IF(psw1="Y",1,0)),0) as psw1,IFNULL(SUM(IF(psw2="Y",1,0)),0) as psw2,IFNULL(SUM(IF(tm1="Y",1,0)),0) as tm1,IFNULL(SUM(IF(tm2="Y",1,0)),0) as tm2,IFNULL(SUM(IF(izin="Y",1,0)),0) as izin');


          $a3=0.5;
          $b3= $get[0]->tl1;
          $c3=($besar_tunjangan*$b3*$a3)/100;

          $a4=1.25;
          $b4= $get[0]->tl2;
          $c4=($besar_tunjangan*$b4*$a4)/100;

          $a5=0.5;
          $b5= $get[0]->psw1;
          $c5=($besar_tunjangan*$b5*$a5)/100;

          $a6=1.25;
          $b6= $get[0]->psw2;
          $c6=($besar_tunjangan*$b6*$a6)/100;


          $a7=3;
          $b7=$get[0]->tm1;
          $c7=($besar_tunjangan*$b7*$a7)/100;

          $a8=2;
          $b8=$get[0]->tm2;
          $c8=($besar_tunjangan*$b8*$a8)/100;

          $a9=1;
          $b9=$get[0]->izin;
          $c9=($besar_tunjangan*$b9*$a9)/100;

          $jumlah_pengurangan=$c1+$c2+$c3+$c4+$c5+$c6+$c7+$c8+$c9;
          $sisa_tunjangan=$besar_tunjangan-$jumlah_pengurangan;
          ?>
          <tr <?=$background;?>>
            <td class="text-center"><?=++$i?></td>
            <td><?=$row->nama_pegawai?></td>
            <td><?=$row->nip?>&nbsp;</td>
            <td class="text-center"><?=$row->status_pegawai?></td>
            <td class="text-center"><?=$row->status_perkawinan?></td>
            <td class="text-center"><?=$row->jumlah_anak?></td>
            <td><?=$row->npwp?>&nbsp;</td>
            <td><?=$row->nama_bank?></td>
            <td>&nbsp;<?=$row->no_rek?></td>
            <td><?=$row->nama_jabatan?></td>
            <td></td>
            <td class="text-center"><?=$row->kelas_jabatan?></td>
            <td><?=uangindonesia($besar_tunjangan)?></td>

            <td><?=$a1?></td>
            <td class="text-center"><?=$b1?></td>
            <td><?=uangindonesia($c1)?></td>

            <?=$kinerjabulanan?>

            <td class="text-center"><?=$a3?></td>
            <td class="bg-kuning text-center"><?=$b3?></td>
            <td><?=uangindonesia($c3)?></td>

            <td class="text-center"><?=$a4?></td>
            <td class="bg-kuning text-center"><?=$b4?></td>
            <td><?=uangindonesia($c4)?></td>

            <td class="text-center"><?=$a5?></td>
            <td class="bg-kuning text-center"><?=$b5?></td>
            <td><?=uangindonesia($c5)?></td>

            <td class="text-center"><?=$a6?></td>
            <td class="bg-kuning text-center"><?=$b6?></td>
            <td><?=uangindonesia($c6)?></td>

            <td class="text-center"><?=$a7?></td>
            <td class="bg-kuning text-center"><?=$b7?></td>
            <td><?=uangindonesia($c7)?></td>

            <td class="text-center"><?=$a8?></td>
            <td class="bg-kuning text-center"><?=$b7?></td>
            <td><?=uangindonesia($c7)?></td>

            <td class="text-center"><?=$a9?></td>
            <td class="bg-kuning text-center"><?=$b9?></td>
            <td><?=uangindonesia($c9)?></td>

            <td><?=uangindonesia($jumlah_pengurangan)?></td>
            <td><?=uangindonesia($sisa_tunjangan)?></td>
            </td>
          </tr>
          <?php
        }
       ?>
    </tbody>
  </table>

</body>
</html>
