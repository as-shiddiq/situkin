<?php $title="Menu Utama"; ?>

<header class="page-header">
  <div class="container-fluid">
    <h2 class="no-margin-bottom">Beranda</h2>
  </div>
</header>
<!-- Breadcrumb-->
<!-- <div class="breadcrumb-holder container-fluid">
  <ul class="breadcrumb">
    <li class="breadcrumb-item"><a href="index.html">Beranda</a></li>
    <li class="breadcrumb-item active">Forms</li>
  </ul>
</div> -->
<section class="forms">
  <div class="container-fluid">
    <?php
    if(Session::get('info')){
      ?>
      <div class="alert alert-success" role="alert">
        <?=Session::get('info');?>
      </div>
      <?php
      Session::destroy('info');
    }
    ?>
    <div class="row">
      <!-- Basic Form-->
      <div class="col-lg-6">
        <div class="card">
          <div class="card-close">
            <div class="dropdown">
              <button type="button" id="closeCard1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-toggle"><i class="fa fa-ellipsis-v"></i></button>
              <div aria-labelledby="closeCard1" class="dropdown-menu dropdown-menu-right has-shadow"><a href="#" class="dropdown-item remove"> <i class="fa fa-times"></i>Close</a><a href="#" class="dropdown-item edit"> <i class="fa fa-gear"></i>Edit</a></div>
            </div>
          </div>
          <div class="card-header d-flex align-items-center">
            <h3 class="h4">Basic Form</h3>
          </div>
          <div class="card-body">
            <p>Lorem ipsum dolor sit amet consectetur.</p>
            <form>
              <div class="form-group">
                <label class="form-control-label">Email</label>
                <input type="email" placeholder="Email Address" class="form-control">
              </div>
              <div class="form-group">
                <label class="form-control-label">Password</label>
                <input type="password" placeholder="Password" class="form-control">
              </div>
              <div class="form-group">
                <input type="submit" value="Signin" class="btn btn-primary">
              </div>
            </form>
          </div>
        </div>
      </div>

        </div>
      </div>
</section>
