<?php
$title="Target";
$url="target";
$table='target';
$primaryKey='id_target';
$fillable=['id_jabatan','target','jenis','komponen_capaian','urutan'];
$id_jabatan='';
$url_clean=$url;
if(isset($_GET['id_jabatan'])){
  Session::set('id_jabatan',$_GET['id_jabatan']);
}
if(Session::get('id_jabatan')){
  $id_jabatan=Session::get('id_jabatan');
}
$url=$url.'&id_jabatan='.$id_jabatan;
if(isset($_POST['post'])){
  if(isset($_POST[$primaryKey])){
    $data=request_all($fillable);
    if($_POST[$primaryKey]==''){
      $exec = $db->insert ($table, $data);
      $info=info_success('Data sukses disimpan');
    }
    else{
      $db->where($primaryKey, $_POST[$primaryKey]);
      $exec = $db->update($table,$data);
      $info=info_success('Data sukses diubah');
    }
  }
  if($exec){
    Session::set('info',$info);
  }
  else{
    Session::set('info',info_success('Maaf terjadi kesalahan'));
  }
  redirect($url.'&id_jabatan='.$_POST['id_jabatan']);
}
elseif(isset($_GET['hapus'])){
  $id=$_GET['id']??'';
  if($id!=''){
    $data=$db->where($primaryKey,$id)->delete($table);
  }
  if($exec){
    Session::set('info',info_success('Data sukses dihapus'));
  }
  else{
    Session::set('info',info_success('Maaf terjadi kesalahan'));
  }
  redirect($url);
}
elseif(isset($_GET['ubah']) OR isset($_GET['tambah'])) {
  $id=$_GET['id']??'';
  if($id!=''){
    $data=$db->ObjectBuilder()->where($primaryKey,$id)->getOne($table);
  }
?>
<section class="content-header">
  <h1>
    Target Kinerja
  </h1>
</section>
<!-- Main content -->
<section class="content">
  <!-- Small boxes (Stat box) -->
  <div class="row">
    <div class="col-md-12">
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Form Target Kinerja</h3>
        </div>
        <form role="form" method="post" action="<?=url($url)?>">
          <div class="box-body">
            <div class="row clearfix">
            <div class="col-md-6">
              <?=input_hidden($primaryKey,($data->$primaryKey??''))?>
              <div class="form-group">
                <label>Urutan</label>
                <?=input_number('urutan',($data->urutan??''));?>
              </div>
              <div class="form-group">
                <label>Nama jabatan</label>
                  <?php
                      $op=array();
                      $op['']='--Pilih salah satu--';
                      $get=$db->ObjectBuilder()->get('jabatan');
                      foreach ($get as $r) {
                        $op[$r->id_jabatan]=$r->nama_jabatan;
                      }
                      echo select('id_jabatan',$op,($data->id_jabatan??$id_jabatan),'','placeholder="--Pilih salah satu--"');?>
              </div>
              <div class="form-group">
                <label>Komponen Capaian</label>
                <?=textarea('komponen_capaian',($data->komponen_capaian??''));?>
              </div>
              <div class="form-group">
                <label>Target</label>
                <?=input_number('target',($data->target??''));?>
              </div>
              <div class="form-group">
                <label>Jenis</label>
                  <?php
                      $op=array();
                      $op['']='--Pilih salah satu--';
                      $op['pokok']='Pokok';
                      $op['tambahan']='Tambahan';
                      echo select('jenis',$op,($data->jenis??''),'','placeholder="--Pilih salah satu--"');?>
              </div>

            </div>
          </div>
          </div>
          <div class="box-footer">
            <button type="submit" name="post" class="btn btn-primary">Simpan</button>
            <a href="<?=url('kelasjabatan')?>" class="btn btn-warning">Kembali</a>
          </div>
        </form>
      </div>
    </div>
  </div>
</section>
<?php } else { ?>
  <section class="content-header">
    <h1>
      Target Kinerja
    </h1>
  </section>
  <!-- Main content -->
  <section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="row">
      <div class="col-md-12">
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">Data Target Kinerja</h3>
          </div>
            <div class="box-body">
              <div class="row">
                <div class="col-md-2">
                  <a href="<?=url($url.'&tambah')?>" class="btn btn-info btn-block">Tambah</a>
                </div>
                <div class="col-md-10">
                    <?php
                        $op=array();
                        $op['']='--Pilih salah satu--';
                        $get=$db->ObjectBuilder()->get('jabatan');
                        foreach ($get as $r) {
                          $op[$r->id_jabatan]=$r->nama_jabatan;
                        }
                        echo select('id_jabatan',$op,$id_jabatan,'jabatan-target','placeholder="--Pilih salah satu--"');?>
                </div>
              </div>
              <hr>
              <?php
              if(Session::get('info')){
                echo Session::get('info');
                Session::destroy('info');
              }
              ?>
               <?php if ($id_jabatan!=''): ?>
              <table class="table table-bordered">
                <thead>
                  <tr>
                    <td width="50px">No</td>
                    <td>Komponen Capaian Prestasi Kinerja Bulanan</td>
                    <td>Target</td>
                    <td width="40px">Edit</td>
                    <td width="40px">Delete</td>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td class="text-center text-bold">I</td>
                    <td class="text-bold">Kegiatan Pokok ( bobot 70% )</td>
                    <td></td>
                    <td></td>
                    <td></td>
                  </tr>
                  <?php
                    $i=0;
                    $db->join("jabatan b", "a.id_jabatan=b.id_jabatan", "LEFT");
                    $db->where('a.id_jabatan',$id_jabatan);
                    $db->where('a.jenis','pokok');
                    $db->orderBy('urutan','ASC');
                    $data=$db->ObjectBuilder()->get($table.' a');
                    foreach ($data as $row) {
                      ?>
                      <tr>
                        <td class="text-center"><?=++$i?></td>
                        <td><?=$row->komponen_capaian?></td>
                        <td><?=$row->target?></td>
                        <td class="text-center">
                          <a href="<?=url($url.'&ubah&id='.$row->$primaryKey)?>" class="btn btn-warning btn-sm"><i class="fa fa-edit"></i> </a>
                        </td>
                        <td class="text-center">
                          <a href="<?=url($url.'&hapus&id='.$row->$primaryKey)?>" class="btn btn-danger btn-sm" onclick="return confirm('Hapus data?')"><i class="fa fa-trash"></i> </a>
                        </td>
                      </tr>
                      <?php
                    }
                   ?>
                   <tr>
                     <td class="text-center text-bold">II</td>
                     <td class="text-bold">Kegiatan Tambahan ( bobot 30% )</td>
                     <td></td>
                     <td></td>
                     <td></td>
                   </tr>
                   <?php
                     $i=0;
                     $db->join("jabatan b", "a.id_jabatan=b.id_jabatan", "LEFT");
                     $db->where('a.id_jabatan',$id_jabatan);
                     $db->where('a.jenis','tambahan');
                     $db->orderBy('urutan','ASC');
                     $data=$db->ObjectBuilder()->get($table.' a');
                     foreach ($data as $row) {
                       ?>
                       <tr>
                         <td class="text-center"><?=++$i?></td>
                         <td><?=$row->komponen_capaian?></td>
                         <td><?=$row->target?></td>
                         <td class="text-center">
                           <a href="<?=url($url.'&ubah&id='.$row->$primaryKey)?>" class="btn btn-warning btn-sm"><i class="fa fa-edit"></i> </a>
                         </td>
                         <td class="text-center">
                           <a href="<?=url($url.'&hapus&id='.$row->$primaryKey)?>" class="btn btn-danger btn-sm" onclick="return confirm('Hapus data?')"><i class="fa fa-trash"></i> </a>
                         </td>
                       </tr>
                       <?php
                     }
                    ?>
                </tbody>
              </table>
            <?php else: ?>
              <?=info_warning('Pilih jabatan terlebih dahulu untuk melihat Komponen Capaian');?>
            <?php endif; ?>
            </div>
            <!-- /.box-body -->
        </div>
      </div>
    </div>
  </section>
<?php  } ?>
