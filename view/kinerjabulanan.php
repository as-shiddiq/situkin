<?php
$title="Kinerja Bulanan";
$url="kinerjabulanan";
$table='kinerja_bulanan';
$table2='kinerja_bulanan_sub';
$primaryKey='id_kinerja_bulanan';
$fillable=['id_pegawai','total_pokok','bobot_pokok','total_tambahan','bobot_tambahan','total_prestasi','penilaian','tanggal_penilaian','pengurangan_tukin','predikat'];
$id_pegawai='';
$bulan=date('m');
$tahun=date('Y');
$url_clean=$url;

$tanggal=date('Y-m');

if(isset($_GET['id_pegawai'])){
  Session::set('id_pegawai',$_GET['id_pegawai']);
}
if(Session::get('id_pegawai')){
  $id_pegawai=Session::get('id_pegawai');
}

if(isset($_GET['bulan'])){
  Session::set('bulan',$_GET['bulan']);
}
if(Session::get('bulan')){
  $bulan=Session::get('bulan');
}

if(isset($_GET['tahun'])){
  Session::set('tahun',$_GET['tahun']);
}
if(Session::get('tahun')){
  $tahun=Session::get('tahun');
}

//ambil info Pegawai
if($id_pegawai!=''){
  $db->join("jabatan b", "a.id_jabatan=b.id_jabatan", "LEFT");
  $getpegawai=$db->ObjectBuilder()->where('id_pegawai',$id_pegawai)->getOne('pegawai a');
  if(count($getpegawai)>0){
    $id_jabatan=$getpegawai->id_jabatan;
    $tanggal_penilaian=$tahun.'-'.$bulan;
    //cek apakah sudah ada data
    $db->where('id_pegawai ='.$id_pegawai);
    $db->where('DATE_FORMAT(tanggal_penilaian,"%Y-%m") ="'.$tanggal_penilaian.'"');
    $cek=$db->ObjectBuilder()->getOne($table);
    if(count($cek)==0){
      // isi karena kosong
      $data['id_pegawai']=$id_pegawai;
      $data['total_pokok']=0;
      $data['bobot_pokok']=0;
      $data['total_tambahan']=0;
      $data['bobot_tambahan']=0;
      $data['total_prestasi']=0;
      $data['penilaian']='E';
      $data['pengurangan_tukin']=20;
      $data['predikat']='E';
      $data['tanggal_penilaian']=$tanggal_penilaian.'-'.date('d');
      $db->insert($table,$data);
      $db->where('id_pegawai ='.$id_pegawai);
      $db->where('DATE_FORMAT(tanggal_penilaian,"%Y-%m") ="'.$tanggal_penilaian.'"');
      $cek=$db->ObjectBuilder()->getOne($table);
      $datakinerja=$cek;
    }
    else{
      $datakinerja=$cek;
    }
  }
  else{
    $id_pegawai='';
  }

}
$url=$url.'&id_pegawai='.$id_pegawai;
if(isset($_POST['post'])){

  $db->where('id_kinerja_bulanan',$_POST['id_kinerja_bulanan'])->delete($table2);

  //insert
  foreach ($_POST['realisasi'] as $id_target => $value) {
    $data=array();
    $data['id_kinerja_bulanan']=$_POST['id_kinerja_bulanan'];
    $data['id_target']=$id_target;
    $data['realisasi']=$_POST['realisasi'][$id_target];
    $data['kinerja']=$_POST['kinerja'][$id_target];
    $data['prosentase']=$_POST['prosentase'][$id_target];
    $exec = $db->insert ($table2, $data);

  }
  $data=array();
  $data=request_all($fillable);
  //hitung pengurangan
  $pengurangan_tukin=0;
  $total_prestasi=$_POST['total_prestasi'];
  if($total_prestasi>=80){
    $pengurangan_tukin=0;
    $predikat='SANGAT BAIK';
  }
  elseif ($total_prestasi>=60) {
    $pengurangan_tukin=5;
    $predikat='BAIK';
  }
  elseif ($total_prestasi>=40) {
    $pengurangan_tukin=10;
    $predikat='CUKUP';
  }
  elseif ($total_prestasi>=20) {
    $pengurangan_tukin=15;
    $predikat='KURANG';
  }
  else{
    $pengurangan_tukin=20;
    $predikat='BURUK';
  }
  $data['pengurangan_tukin']=$pengurangan_tukin;
  $data['predikat']=$predikat;
  $db->where($primaryKey, $_POST[$primaryKey]);
  $exec = $db->update($table,$data);
  $info=info_success('Kinerja bulanan sukses disesuaikan');

  if($exec){
    Session::set('info',$info);
  }
  else{
    Session::set('info',info_success('Maaf terjadi kesalahan'));
  }
  redirect($url.'&id_pegawai='.$_POST['id_pegawai']);
}
elseif(isset($_GET['hapus'])){
  $id=$_GET['id']??'';
  if($id!=''){
    $data=$db->where($primaryKey,$id)->delete($table);
  }
  if($exec){
    Session::set('info',info_success('Data sukses dihapus'));
  }
  else{
    Session::set('info',info_success('Maaf terjadi kesalahan'));
  }
  redirect($url);
}
else{
?>
  <section class="content-header">
    <h1>
      Kinerja Bulanan
    </h1>
  </section>
  <!-- Main content -->
  <section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="row">
      <div class="col-md-12">
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">Data Capaian Kinerja</h3>
          </div>
            <div class="box-body">
              <div class="row">
                <form action="<?=url($url)?>">
                  <input type="hidden" name="view" value="<?=$url_clean?>">
                  <div class="col-md-6">
                      <?php
                          $op=array();
                          $op['']='--Pilih salah satu--';
                          $get=$db->ObjectBuilder()->get('pegawai');
                          foreach ($get as $r) {
                            $op[$r->id_pegawai]=$r->nama_pegawai;
                          }
                          echo select('id_pegawai',$op,$id_pegawai,'pegawai-capaian','placeholder="--Pilih salah satu--"');?>
                  </div>
                  <div class="col-md-2">
                      <?php
                          $op=array();
                          $op['']='--Pilih salah satu--';
                          for ($i=1; $i <13 ; $i++) {
                            if(strlen($i)==1){
                              $i='0'.$i;
                            }
                            $op[$i]=bulan_huruf($i);
                          }
                          echo select('bulan',$op,$bulan,'pegawai-capaian','placeholder="--Pilih salah satu--"');?>
                  </div>
                  <div class="col-md-2">
                      <?php
                          $op=array();
                          $op['']='--Pilih salah satu--';
                          for ($i=date('Y'); $i >date('Y')-10 ; $i--) {
                            $op[$i]=$i;
                          }
                          echo select('tahun',$op,$tahun,'pegawai-capaian','placeholder="--Pilih salah satu--"');?>
                  </div>
                  <div class="col-md-2">
                    <button type="submit" class="btn btn-block btn-primary btn-flat">Tampilkan</button>
                  </div>
                </form>
              </div>
              <?php
              if(Session::get('info')){
                echo Session::get('info');
                Session::destroy('info');
              }
              ?>
               <?php if ($id_pegawai!=''): ?>
               <?php if (!isset($_GET['atur'])): ?>
                 <hr>
                 <a href="<?=url($url.'&atur')?>" class="btn btn-warning">Atur Capaian</a>
               <?php endif;?>
              <hr>
              <h3 class="text-center">LAPORAN CAPAIAN KINERJA BULANAN PEGAWAI</h3>
              <h3 class="text-center">RRI BANJARMASIN</h3>
               <table class="table">
                 <tr>
                   <td width="150">Nama</td>
                   <td width="10">:</td>
                   <td><?=$getpegawai->nama_pegawai?></td>
                 </tr>
                 <tr>
                   <td>NIP</td>
                   <td>:</td>
                   <td><?=$getpegawai->nip?></td>
                 </tr>
                 <tr>
                   <td>Jabatan</td>
                   <td>:</td>
                   <td><?=$getpegawai->nama_jabatan?></td>
                 </tr>
               </table>
               <form action="<?=url($url)?>" method="post">
              <?=input_hidden('id_kinerja_bulanan',$datakinerja->id_kinerja_bulanan)?>
              <?=input_hidden('id_pegawai',$id_pegawai)?>
              <?=input_hidden('tanggal_penilaian',$tanggal_penilaian.'-'.date('d'))?>
              <table class="table table-bordered">
                <thead class="valign-middle text-center">
                  <tr>
                    <td width="50px" rowspan="2">No</td>
                    <td rowspan="2">Komponen Capaian Prestasi Kinerja Bulanan</td>
                    <td colspan="3">Pengukuran Kinerja</td>
                    <td rowspan="2" width="100">Prosentase Capaian Kinerja</td>
                  </tr>
                  <tr>
                    <td width="100">Target</td>
                    <td width="100">Realisasi</td>
                    <td width="100">Kinerja</td>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td class="text-center text-bold">I</td>
                    <td class="text-bold">Kegiatan Pokok ( bobot 70% )</td>
                    <td></td>
                    <td></td>
                    <td></td>
                  </tr>
                  <?php
                    $i=0;
                    $db->join("jabatan b", "a.id_jabatan=b.id_jabatan", "LEFT");
                    $db->where('a.id_jabatan',$id_jabatan);
                    $db->where('a.jenis','pokok');
                    $db->orderBy('urutan','ASC');
                    $data=$db->ObjectBuilder()->get('target a');
                    foreach ($data as $row) {
                      $db->where('id_kinerja_bulanan ='.$datakinerja->id_kinerja_bulanan);
                      $db->where('id_target ='.$row->id_target);
                      $datasub=$db->ObjectBuilder()->getOne($table2);
                      ?>
                      <tr>
                        <td class="text-center"><?=++$i?></td>
                        <td><?=$row->komponen_capaian?></td>
                        <td class="text-center"><?=$row->target?></td>
                        <?php if (isset($_GET['atur'])): ?>
                          <?=input_hidden('target',$row->target,'target-'.$row->id_target)?>
                          <td><?=input_number('realisasi['.$row->id_target.']',($datasub->realisasi??0),'p-realisasi','max="'.$row->target.'" data-prealisasi="'.$row->id_target.'"')?></td>
                          <td><?=input_number('kinerja['.$row->id_target.']',($datasub->kinerja??0),'p-kinerja','tabIndex="-1" readonly data-pkinerja="'.$row->id_target.'"')?></td>
                          <td><?=input_number('prosentase['.$row->id_target.']',($datasub->prosentase??0),'p-prosentase','tabIndex="-1" readonly data-pprosentase="'.$row->id_target.'"')?></td>
                        <?php else: ?>
                          <td class="text-center"><?=($datasub->realisasi??0);?></td>
                          <td class="text-center"><?=($datasub->kinerja??0);?></td>
                          <td class="text-center"><?=($datasub->prosentase??0);?></td>
                        <?php endif; ?>
                      </tr>
                      <?php
                    }
                   ?>
                   <tr>
                     <td></td>
                     <td>Sub Total Kinerja Capaian Bulanan dari Kegiatan Pokok</td>
                     <td></td>
                     <td></td>
                     <td></td>
                     <?php if (isset($_GET['atur'])): ?>
                       <td><?=input_number('total_pokok',($datakinerja->total_pokok??0),'total_pokok','readonly')?></td>
                     <?php else:?>
                       <td class="text-center"><?=$datakinerja->total_pokok?></td>
                     <?php endif;?>
                   </tr>
                   <tr>
                     <td></td>
                     <td>Presentasi Nilai Pembobotan</td>
                     <td></td>
                     <td></td>
                     <td></td>
                     <?php if (isset($_GET['atur'])): ?>
                       <td><?=input_number('bobot_pokok',($datakinerja->bobot_pokok??0),'bobot_pokok','readonly')?></td>
                     <?php else:?>
                       <td class="text-center"><?=$datakinerja->bobot_pokok?></td>
                     <?php endif;?>
                   </tr>
                   <tr>
                     <td class="text-center text-bold">II</td>
                     <td class="text-bold">Kegiatan Tambahan ( bobot 30% )</td>
                     <td></td>
                     <td></td>
                     <td></td>
                   </tr>
                   <?php
                     $i=0;
                     $db->join("jabatan b", "a.id_jabatan=b.id_jabatan", "LEFT");
                     $db->where('a.id_jabatan',$id_jabatan);
                     $db->where('a.jenis','tambahan');
                     $db->orderBy('urutan','ASC');
                     $data=$db->ObjectBuilder()->get('target a');
                     foreach ($data as $row) {
                       $db->where('id_kinerja_bulanan ='.$datakinerja->id_kinerja_bulanan);
                       $db->where('id_target ='.$row->id_target);
                       $datasub=$db->ObjectBuilder()->getOne($table2);
                       ?>
                       <tr>
                         <td class="text-center"><?=++$i?></td>
                         <td><?=$row->komponen_capaian?></td>
                         <td class="text-center"><?=$row->target?></td>
                         <?php if (isset($_GET['atur'])): ?>
                           <?=input_hidden('target',$row->target,'target-'.$row->id_target)?>
                           <td><?=input_number('realisasi['.$row->id_target.']',($datasub->realisasi??0),'t-realisasi','max="'.$row->target.'" data-trealisasi="'.$row->id_target.'"')?></td>
                           <td><?=input_number('kinerja['.$row->id_target.']',($datasub->kinerja??0),'t-kinerja','tabIndex="-1" readonly data-tkinerja="'.$row->id_target.'"')?></td>
                           <td><?=input_number('prosentase['.$row->id_target.']',($datasub->prosentase??0),'t-prosentase','tabIndex="-1" readonly data-tprosentase="'.$row->id_target.'"')?></td>
                         <?php else: ?>
                           <td class="text-center"><?=($datasub->realisasi??0);?></td>
                           <td class="text-center"><?=($datasub->kinerja??0);?></td>
                           <td class="text-center"><?=($datasub->prosentase??0);?></td>
                         <?php endif; ?>
                       </tr>
                       <?php
                     }
                    ?>

                    <tr>
                      <td></td>
                      <td>Sub Total Kinerja Capaian Bulanan dari Kegiatan Tambahan</td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <?php if (isset($_GET['atur'])): ?>
                        <td><?=input_number('total_tambahan',($datakinerja->total_tambahan??0),'total_tambahan','readonly')?></td>
                      <?php else:?>
                        <td class="text-center"><?=$datakinerja->total_tambahan?></td>
                      <?php endif;?>
                    </tr>
                    <tr>
                      <td></td>
                      <td>Presentasi Nilai Pembobotan</td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <?php if (isset($_GET['atur'])): ?>
                        <td><?=input_number('bobot_tambahan',($datakinerja->bobot_tambahan??0),'bobot_tambahan','readonly')?></td>
                      <?php else:?>
                        <td class="text-center"><?=$datakinerja->bobot_tambahan?></td>
                      <?php endif;?>
                    </tr>
                    <tr>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr class="text-bold">
                      <td></td>
                      <td>Total Prestasi Kerja Bulanan</td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <?php if (isset($_GET['atur'])): ?>
                        <td><?=input_number('total_prestasi',($datakinerja->total_prestasi??0),'total_prestasi','readonly')?></td>
                      <?php else:?>
                        <td class="text-center"><?=$datakinerja->total_prestasi?></td>
                      <?php endif;?>
                    </tr>
                    <tr class="text-bold">
                      <td></td>
                      <td>Penilaian</td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <?php if (isset($_GET['atur'])): ?>
                        <td><?=input_text('penilaian',($datakinerja->penilaian??0),'penilaian','readonly')?></td>
                      <?php else:?>
                        <td class="text-center"><?=$datakinerja->penilaian?></td>
                      <?php endif;?>
                    </tr>
                </tbody>
              </table>
              <?php if (isset($_GET['atur'])): ?>
                <button type="submit" name="post" class="btn btn-primary">Simpan</button>
                <a href="<?=url($url)?>" class="btn btn-warning">Kembali</a>
              <?php endif;?>
            </form>
            <?php else: ?>
              <?=info_warning('Pilih pegawai terlebih dahulu untuk melihat Komponen Capaian');?>
            <?php endif; ?>
            </div>
            <!-- /.box-body -->
        </div>
      </div>
    </div>
  </section>
<?php  } ?>
