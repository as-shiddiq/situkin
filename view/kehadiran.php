<?php
$title="Kehadiran";
$url="kehadiran";
$table='kehadiran';
$primaryKey='id_kehadiran';
$fillable=['id_pegawai','tanggal_kehadiran','tl1','tl2','psw1','psw2','tm1','tm2','izin'];
if(isset($_POST['post'])){
  if(isset($_POST[$primaryKey])){
    $data=request_all($fillable);
    if($_POST[$primaryKey]==''){
      $exec = $db->insert ($table, $data);
      $info=info_success('Data sukses disimpan');
    }
    else{
      $db->where($primaryKey, $_POST[$primaryKey]);
      $exec = $db->update($table,$data);
      $info=info_success('Data sukses diubah');
    }
  }
  if($exec){
    Session::set('info',$info);
  }
  else{
    Session::set('info',info_success('Maaf terjadi kesalahan'));
  }
  redirect($url);
}
elseif(isset($_GET['hapus'])){
  $id=$_GET['id']??'';
  if($id!=''){
    $data=$db->where($primaryKey,$id)->delete($table);
  }
  if($exec){
    Session::set('info',info_success('Data sukses dihapus'));
  }
  else{
    Session::set('info',info_success('Maaf terjadi kesalahan'));
  }
  redirect($url);
}
elseif(isset($_GET['ubah']) OR isset($_GET['tambah'])) {
  $id=$_GET['id']??'';
  if($id!=''){
    $data=$db->ObjectBuilder()->where($primaryKey,$id)->getOne($table);
  }
?>
<section class="content-header">
  <h1>
    Kehadiran
  </h1>
</section>
<!-- Main content -->
<section class="content">
  <!-- Small boxes (Stat box) -->
  <div class="row">
    <div class="col-md-12">
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Kehadiran</h3>
        </div>
        <form role="form" method="post" action="<?=url($url)?>">
          <div class="box-body">
            <div class="row clearfix">
            <div class="col-md-6">
              <?=input_hidden($primaryKey,($data->$primaryKey??''))?>
              <div class="form-group">
                <label>Nama Pegawai</label>
                  <?php
                    $op=array();
                    $op['']='--Pilih salah satu--';
                    $get=$db->ObjectBuilder()->get('pegawai');
                    foreach ($get as $r) {
                      $op[$r->id_pegawai]=$r->nama_pegawai;
                    }
                    echo select('id_pegawai',$op,($data->id_pegawai??''),'');?>

              </div>
              <div class="form-group">
                <label>Tanggal</label>
                  <?=input_date('tanggal_kehadiran',($data->tanggal_kehadiran??date('Y-m-d')),'');?>
              </div>
              <h4>Keterangan <small>(Contreng jika diperlukan)</small>:</h4>
              <table class="table">
                <tr>
                  <td width="10px">
                    <?=checkbox('tl1','Y',($data->tl1??''),'','id="tl1"');?>
                  </td>
                  <td><label for="tl1">Terlambat 1 Menit s.d. 60 Menit (TL1)</label></td>
                </tr>
                <tr>
                  <td>
                    <?=checkbox('tl2','Y',($data->tl2??''),'','id="tl2"');?>
                  </td>
                  <td><label for="tl2">Terlambat > 60 menit dan tidak mengganti waktu keterlambatan (TL2) </label></td>
                </tr>
                <tr>
                  <td>
                    <?=checkbox('psw1','Y',($data->psw1??''),'','id="psw1"');?>
                  </td>
                  <td><label for="psw1">Pulang Kerja 1 menit s.d. 60 menit sebelum waktu pulang (PSW1)</label></td>
                </tr>
                <tr>
                  <td>
                    <?=checkbox('psw2','Y',($data->psw2??''),'','id="psw2"');?>
                  </td>
                  <td><label for="psw2">Pulang Kerja lebih dari 60 menit sebelum waktu pulang (PSW2)</label></td>
                </tr>
                <tr>
                  <td>
                    <?=checkbox('tm1','Y',($data->tm1??''),'','id="tm1"');?>
                  </td>
                  <td><label for="tm1">Tidak Masuk Kerja tanpa alasan sah</label></td>
                </tr>
                <tr>
                  <td>
                    <?=checkbox('tm2','Y',($data->tm2??''),'','id="tm2"');?>
                  </td>
                  <td><label for="tm2">Tidak Masuk Kerja dengan seijin atasan</label></td>
                </tr>
                <tr>
                  <td>
                    <?=checkbox('izin','Y',($data->izin??''),'','id="izin"');?>
                  </td>
                  <td><label for="izin">Sakit, cuti besar, cuti bersalin, dan/atau cuti karena alasan penting</label></td>
                </tr>
              </table>
            </div>
          </div>
          </div>
          <div class="box-footer">
            <button type="submit" name="post" class="btn btn-primary">Simpan</button>
            <a href="<?=url('kelasjabatan')?>" class="btn btn-warning">Kembali</a>
          </div>
        </form>
      </div>
    </div>
  </div>
</section>
<?php } else {
  $id_pegawai=($_GET['id_pegawai']??'');
?>
  <section class="content-header">
    <h1>
      Kehadiran
    </h1>
  </section>
  <!-- Main content -->
  <section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="row">
      <div class="col-md-12">
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">Data Kehadiran</h3>
          </div>
            <div class="box-body">
              <div class="row">
              <div class="col-md-4">
                <a href="<?=url($url.'&tambah')?>" class="btn btn-info">Tambah</a>
              </div>
              <div class="col-md-8">
                <form class="" action="<?=url($url)?>" method="get">
              <div class="input-group">
                <input type="hidden" name="view" value="<?=$url;?>">
                <?php
                    $op=array();
                    $op['']='--Semua--';
                    $get=$db->ObjectBuilder()->get('pegawai');
                    foreach ($get as $r) {
                      $op[$r->id_pegawai]=$r->nama_pegawai;
                    }
                    echo select('id_pegawai',$op,($id_pegawai),'');?>
                   <span class="input-group-btn">
                     <button type="submit" class="btn btn-primary btn-flat">Tampilkan</button>
                   </span>
                 </div>
               </form>
               </div>
             </div>
              <hr>
              <?php
              if(Session::get('info')){
                echo Session::get('info');
                Session::destroy('info');
              }
              ?>
              <table class="table table-bordered dt">
                <thead>
                  <tr>
                    <td width="50px">No</td>
                    <td>NIP</td>
                    <td>Nama</td>
                    <td>Jabatan</td>
                    <td>TL1</td>
                    <td>TL2</td>
                    <td>PSW1</td>
                    <td>PSW2</td>
                    <td>TM1</td>
                    <td>TM2</td>
                    <td>Izin/Cuti</td>
                    <td width="40px">Edit</td>
                    <td width="40px">Delete</td>
                  </tr>
                </thead>
                <tbody>
                  <?php
                    $i=0;
                    if($id_pegawai!=''){
                      $db->where('a.id_pegawai',$id_pegawai);
                    }
                    $db->join("pegawai b", "a.id_pegawai=b.id_pegawai", "LEFT");
                    $db->join("jabatan c", "b.id_jabatan=c.id_jabatan", "LEFT");
                    $data=$db->ObjectBuilder()->get($table.' a');
                    foreach ($data as $row) {
                      ?>
                      <tr>
                        <td class="text-center"><?=++$i?></td>
                        <td><?=$row->nip?></td>
                        <td><?=$row->nama_pegawai?></td>
                        <td><?=$row->nama_jabatan?></td>
                        <td class="text-center"><?=$row->tl1?></td>
                        <td class="text-center"><?=$row->tl2?></td>
                        <td class="text-center"><?=$row->psw1?></td>
                        <td class="text-center"><?=$row->psw2?></td>
                        <td class="text-center"><?=$row->tm1?></td>
                        <td class="text-center"><?=$row->tm2?></td>
                        <td class="text-center"><?=$row->izin?></td>
                        <td class="text-center">
                          <a href="<?=url($url.'&ubah&id='.$row->$primaryKey)?>" class="btn btn-warning btn-sm"><i class="fa fa-edit"></i> </a>
                        </td>
                        <td class="text-center">
                          <a href="<?=url($url.'&hapus&id='.$row->$primaryKey)?>" class="btn btn-danger btn-sm" onclick="return confirm('Hapus data?')"><i class="fa fa-trash"></i> </a>
                        </td>
                      </tr>
                      <?php
                    }
                   ?>
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
        </div>
      </div>
    </div>
  </section>
<?php  } ?>
