<!-- jQuery 2.2.3 -->
<script src="<?=templates()?>plugins/jQuery/jquery-2.2.3.min.js"></script>
<script src="<?=assets()?>js/jquery-ui.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<!-- <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script> -->
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.6 -->
<script src="<?=templates()?>bootstrap/js/bootstrap.min.js"></script>
<!-- Morris.js charts -->
<script src="<?=templates()?>plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?=templates()?>plugins/datatables/dataTables.bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<script src="<?=templates()?>plugins/morris/morris.min.js"></script>
<!-- Sparkline -->
<script src="<?=templates()?>plugins/sparkline/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<!-- daterangepicker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<script src="<?=templates()?>plugins/daterangepicker/daterangepicker.js"></script>
<!-- datepicker -->
<script src="<?=templates()?>plugins/datepicker/bootstrap-datepicker.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<!-- Slimscroll -->
<script src="<?=templates()?>plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="<?=templates()?>plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="<?=templates()?>dist/js/app.min.js"></script>
<script src="<?=assets()?>js/DecimalAdjust.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->

<script>

  $(function () {
    $('.dt').DataTable({
      "paging": true,
      "lengthChange": false,
      "ordering": true,
      "info": true,
      "autoWidth": false
    });
  });

  $('.jabatan-target').change(function(){
    $val=$(this).val();
    window.location='<?=url(($url_clean??''))?>&id_jabatan='+$val;
  })

  $(".p-realisasi").change(function(){
    $val=$(this).val();
    $pos=$(this).data('prealisasi');
    $target=$('.target-'+$pos).val();
    $kinerja=parseFloat($val)/parseFloat($target);
    $prosentase=$kinerja*100;
    if(parseFloat($target)<parseFloat($val)){
        alert('Realisasi maksimal '+$target);
        $(this).val($target);
        $kinerja=1;
    }
    $("[data-pkinerja="+$pos+"]").val(Math.round10($kinerja,-2));
    $("[data-pprosentase="+$pos+"]").val(Math.round10($prosentase,-2));
    sumpokok();
  }).keyup(function(){
    $(this).change();
  });

  $(".t-realisasi").change(function(){
    $val=$(this).val();
    $pos=$(this).data('trealisasi');
    $target=$('.target-'+$pos).val();
    $kinerja=parseFloat($val)/parseFloat($target);
    if(parseFloat($target)==0){
      $kinerja=1;
    }
    $prosentase=$kinerja*100;
    if(parseFloat($target)<parseFloat($val)){
        alert('Realisasi maksimal '+$target);
        $(this).val($target);
        $kinerja=1;
    }
    $("[data-tkinerja="+$pos+"]").val(Math.round10($kinerja,-2));
    $("[data-tprosentase="+$pos+"]").val(Math.round10($prosentase,-2));
    sumtambahan();
  }).keyup(function(){
    $(this).change();
  });

  function sumpokok(){
    var sum = 0;
    $total=0;
    $('.p-prosentase').each(function(){
        $total++;
        sum += parseFloat($(this).val());
    });
    $total_pokok=sum/$total;
    $bobot_pokok=($total_pokok*70)/100;
    $('.total_pokok').val(Math.round10($total_pokok,-2));
    $('.bobot_pokok').val(Math.round10($bobot_pokok,-2));
    hitungtotalprestasi();
  }

  function sumtambahan(){
    var sum = 0;
    $total=0;
    $('.t-prosentase').each(function(){
        $total++;
        sum += parseFloat($(this).val());
    });
    $total_pokok=sum/$total;
    $bobot_pokok=($total_pokok*30)/100;
    $('.total_tambahan').val(Math.round10($total_pokok,-2));
    $('.bobot_tambahan').val(Math.round10($bobot_pokok,-2));
    hitungtotalprestasi();
  }
  function hitungtotalprestasi(){
    $p=$('.bobot_pokok').val();
    $t=$('.bobot_tambahan').val();
    $total_prestasi=parseFloat($p)+parseFloat($t);
    $penilaian='J';
    if($total_prestasi>=90){
      $penilaian='A';
    }
    else if($total_prestasi>=80){
      $penilaian='B';
    }
    else if($total_prestasi>=70){
      $penilaian='C';
    }
    else if($total_prestasi>=60){
      $penilaian='D';
    }
    else if($total_prestasi>=50){
      $penilaian='E';
    }
    else if($total_prestasi>=40){
      $penilaian='F';
    }
    else if($total_prestasi>=30){
      $penilaian='G';
    }
    else if($total_prestasi>=20){
      $penilaian='H';
    }
    else if($total_prestasi>=10){
      $penilaian='I';
    }
    $(".total_prestasi").val($total_prestasi);
    $(".penilaian").val($penilaian);
  }
</script>
