<aside class="main-sidebar">
  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">
    <!-- Sidebar user panel -->
    <div class="user-panel">
      <div class="pull-left image">
        <img src="<?=templates()?>dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
      </div>
      <div class="pull-left info">
        <p><?=Session::get('nama_lengkap');?></p>
        <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
      </div>
    </div>
    <!-- sidebar menu: : style can be found in sidebar.less -->
    <ul class="sidebar-menu">
      <li class="header">MAIN NAVIGATION</li>
      <li>
        <a href="<?=url('home')?>">
          <i class="fa fa-dashboard"></i> <span>Dashboard</span>
        </a>
      </li>
      <li class="treeview">
        <a href="#">
          <i class="fa fa-folder"></i> <span>Master Data</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li><a href="<?=url('kelasjabatan')?>"><i class="fa fa-circle-o"></i> Kelas Jabatan</a></li>
          <li><a href="<?=url('unitkerja')?>"><i class="fa fa-circle-o"></i> Unit Kerja</a></li>
          <li><a href="<?=url('jabatan')?>"><i class="fa fa-circle-o"></i> Jabatan</a></li>
          <li><a href="<?=url('target')?>"><i class="fa fa-circle-o"></i> Target Kinerja</a></li>
          <li><a href="<?=url('bank')?>"><i class="fa fa-circle-o"></i> Daftar Bank</a></li>
          <li><a href="<?=url('pegawai')?>"><i class="fa fa-circle-o"></i> Pegawai</a></li>
        </ul>
      </li>
      <li>
        <a href="<?=url('kinerjabulanan')?>">
          <i class="fa fa-certificate"></i> <span>Prestasi Kerja Bulanan</span>
        </a>
      </li>
      <li>
        <a href="<?=url('kehadiran')?>">
          <i class="fa fa-bar-chart"></i> <span>Kehadiran</span>
        </a>
      </li>
      <li>
        <a href="<?=url('tukin')?>">
          <i class="fa fa-print"></i> <span>Tunjangan Kinerja</span>
        </a>
      </li>
      <li>
        <a href="<?=url('logout')?>" class="text-red">
          <i class="fa fa-sign-out"></i> <span>Keluar</span>
        </a>
      </li>

    </ul>
  </section>
  <!-- /.sidebar -->
</aside>
