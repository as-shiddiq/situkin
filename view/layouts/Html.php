<!DOCTYPE html>
<html>
<head>
  <?php include 'head.php';?>
</head>
<body class="hold-transition skin-red sidebar-mini">
<div class="wrapper">
  <?php include 'header.php';?>
  <?php include 'sidebar.php';?>

  <div class="content-wrapper">
    <?=$view?>
  </div>
  <?php include 'footer.php';?>
</div>
<?php include 'javascript.php';?>

</body>
</html>
