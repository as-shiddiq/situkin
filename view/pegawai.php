<?php
$title="Data Pegawai";
$url="pegawai";
$table='pegawai';
$primaryKey='id_pegawai';
$fillable=['id_bank','id_jabatan','nip','nama_pegawai','status_pegawai','status_perkawinan','jumlah_anak','npwp','no_rek','pangkat_gol','tmt'];
if(isset($_POST['post'])){
  if(isset($_POST[$primaryKey])){
    $data=request_all($fillable);
    if($_POST[$primaryKey]==''){
      $exec = $db->insert ($table, $data);
      $info=info_success('Data sukses disimpan');
    }
    else{
      $db->where($primaryKey, $_POST[$primaryKey]);
      $exec = $db->update($table,$data);
      $info=info_success('Data sukses diubah');
    }
  }
  if($exec){
    Session::set('info',$info);
  }
  else{
    Session::set('info',info_success('Maaf terjadi kesalahan'));
  }
  redirect($url);
}
elseif(isset($_GET['hapus'])){
  $id=$_GET['id']??'';
  if($id!=''){
    $data=$db->where($primaryKey,$id)->delete($table);
  }
  if($exec){
    Session::set('info',info_success('Data sukses dihapus'));
  }
  else{
    Session::set('info',info_success('Maaf terjadi kesalahan'));
  }
  redirect($url);
}
elseif(isset($_GET['ubah']) OR isset($_GET['tambah'])) {
  $id=$_GET['id']??'';
  if($id!=''){
    $data=$db->ObjectBuilder()->where($primaryKey,$id)->getOne($table);
  }
?>
<section class="content-header">
  <h1>
    Pegawai
  </h1>
</section>
<!-- Main content -->
<section class="content">
  <!-- Small boxes (Stat box) -->
  <div class="row">
    <div class="col-md-12">
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Form Pegawai</h3>
        </div>
        <form role="form" method="post" action="<?=url($url)?>">
          <div class="box-body">
            <div class="row clearfix">
            <div class="col-md-6">
              <?=input_hidden($primaryKey,($data->$primaryKey??''))?>
              <div class="form-group">
                <label>NIP</label>
                <?=input_text('nip',($data->nip??''))?>
              </div>
              <div class="form-group">
                <label>Nama Pegawai</label>
                <?=input_text('nama_pegawai',($data->nama_pegawai??''))?>
              </div>
              <div class="form-group">
                <label>Status Pegawai</label>
                  <?php
                      $op=array();
                      echo input_text('status_pegawai',($data->status_pegawai??''),'');?>
              </div>
              <div class="form-group">
                <label>TMT</label>
                  <?php
                      echo input_date('tmt',($data->tmt??''),'');?>
              </div>
              <div class="form-group">
                <label>Pangkat/Golongan</label>
                  <?php
                    $op=array();
                    $op['']='--Pilih salah satu--';
                    $op=array_merge($op,pangkatgol());
                    echo select('pangkat_gol',$op,($data->pangkat_gol??''),'','placeholder="--Pilih salah satu--"');?>
              </div>
              <div class="form-group">
                <label>Jabatan</label>
                  <?php
                    $op=array();
                    $op['']='--Pilih salah satu--';
                    $get=$db->ObjectBuilder()->get('jabatan');
                    foreach ($get as $r) {
                      $op[$r->id_jabatan]=$r->nama_jabatan;
                    }
                    echo select('id_jabatan',$op,($data->id_jabatan??''),'','placeholder="--Pilih salah satu--"');?>
              </div>
              <div class="form-group">
                <label>Status Perkawinan/Jumlah Anak</label>
                <div class="row">
                  <div class="col-md-3">
                  <?php
                      $op=array();
                      $op['']='--Pilih salah satu--';
                      $op['TK']='Tidak Kawin';
                      $op['K']='Kawin';
                      echo select('status_perkawinan',$op,($data->status_perkawinan??''),'','placeholder="--Pilih salah satu--"');?>
                  </div>
                  <div class="col-md-9">
                    <?=input_text('jumlah_anak',($data->jumlah_anak??''));?>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <label>NPWP</label>
                  <?php
                      $op=array();
                      echo input_text('npwp',($data->npwp??''),'');?>
              </div>
              <div class="form-group">
                <label>Bank/No. Rekening</label>
                <div class="row">
                  <div class="col-md-6">
                  <?php
                      $op=array();
                      $op['']='--Pilih salah satu--';
                      $get=$db->ObjectBuilder()->get('bank');
                      foreach ($get as $r) {
                        $op[$r->id_bank]=$r->nama_bank;
                      }
                      echo select('id_bank',$op,($data->id_bank??''),'','placeholder="--Pilih salah satu--"');?>
                  </div>
                  <div class="col-md-6">
                    <?=input_text('no_rek',($data->no_rek??''));?>
                  </div>
                </div>
              </div>
            </div>
          </div>
          </div>
          <div class="box-footer">
            <button type="submit" name="post" class="btn btn-primary">Simpan</button>
            <a href="<?=url($url)?>" class="btn btn-warning">Kembali</a>
          </div>
        </form>
      </div>
    </div>
  </div>
</section>
<?php } else { ?>
  <section class="content-header">
    <h1>
      Pegawai
    </h1>
  </section>
  <!-- Main content -->
  <section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="row">
      <div class="col-md-12">
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">Data Pegawai</h3>
          </div>
            <div class="box-body">
              <a href="<?=url($url.'&tambah')?>" class="btn btn-info">Tambah</a>
              <hr>
              <?php
              if(Session::get('info')){
                echo Session::get('info');
                Session::destroy('info');
              }
              ?>
              <table class="table table-bordered dt">
                <thead>
                  <tr>
                    <td width="50px">No</td>
                    <td>NIP</td>
                    <td>Nama</td>
                    <td>Jabatan</td>
                    <td>Status Pegawai</td>
                    <td>Pangkat/Gol</td>
                    <td>NPWP</td>
                    <td>No. Rek</td>
                    <td width="40px">Edit</td>
                    <td width="40px">Delete</td>
                  </tr>
                </thead>
                <tbody>
                  <?php
                    $i=0;
                    $db->join("bank b", "a.id_bank=b.id_bank", "LEFT");
                    $db->join("jabatan c", "a.id_jabatan=c.id_jabatan", "LEFT");
                    $data=$db->ObjectBuilder()->get($table.' a');
                    foreach ($data as $row) {
                      ?>
                      <tr>
                        <td class="text-center"><?=++$i?></td>
                        <td><?=$row->nip?></td>
                        <td><?=$row->nama_pegawai?></td>
                        <td><?=$row->nama_jabatan?></td>
                        <td><?=$row->status_pegawai?></td>
                        <td><?=$row->pangkat_gol?></td>
                        <td><?=$row->npwp?></td>
                        <td><?=$row->no_rek?></td>
                        <td class="text-center">
                          <a href="<?=url($url.'&ubah&id='.$row->$primaryKey)?>" class="btn btn-warning btn-sm"><i class="fa fa-edit"></i> </a>
                        </td>
                        <td class="text-center">
                          <a href="<?=url($url.'&hapus&id='.$row->$primaryKey)?>" class="btn btn-danger btn-sm" onclick="return confirm('Hapus data?')"><i class="fa fa-trash"></i> </a>
                        </td>
                      </tr>
                      <?php
                    }
                   ?>
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
        </div>
      </div>
    </div>
  </section>
<?php  } ?>
