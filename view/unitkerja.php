<?php
$title="Unit Kerja";
$url="unitkerja";
$table='unit_kerja';
$primaryKey='id_unit_kerja';
$fillable=['unit_kerja'];
if(isset($_POST['post'])){
  if(isset($_POST[$primaryKey])){
    $data=request_all($fillable);
    if($_POST[$primaryKey]==''){
      $exec = $db->insert ($table, $data);
      $info=info_success('Data sukses disimpan');
    }
    else{
      $db->where($primaryKey, $_POST[$primaryKey]);
      $exec = $db->update($table,$data);
      $info=info_success('Data sukses diubah');
    }
  }
  if($exec){
    Session::set('info',$info);
  }
  else{
    Session::set('info',info_success('Maaf terjadi kesalahan'));
  }
  redirect($url);
}
elseif(isset($_GET['hapus'])){
  $id=$_GET['id']??'';
  if($id!=''){
    $data=$db->where($primaryKey,$id)->delete($table);
  }
  if($exec){
    Session::set('info',info_success('Data sukses dihapus'));
  }
  else{
    Session::set('info',info_success('Maaf terjadi kesalahan'));
  }
  redirect($url);
}
elseif(isset($_GET['ubah']) OR isset($_GET['tambah'])) {
  $id=$_GET['id']??'';
  if($id!=''){
    $data=$db->ObjectBuilder()->where($primaryKey,$id)->getOne($table);
  }
?>
<section class="content-header">
  <h1>
    Unit Kerja
  </h1>
</section>
<!-- Main content -->
<section class="content">
  <!-- Small boxes (Stat box) -->
  <div class="row">
    <div class="col-md-12">
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Form Unit Kerja</h3>
        </div>
        <form role="form" method="post" action="<?=url($url)?>">
          <div class="box-body">
            <div class="row clearfix">
            <div class="col-md-6">
              <?=input_hidden($primaryKey,($data->$primaryKey??''))?>
              <div class="form-group">
                <label>Unit Kerja</label>
                <?=input_text('unit_kerja',($data->unit_kerja??''))?>
              </div>
            </div>
          </div>
          </div>
          <div class="box-footer">
            <button type="submit" name="post" class="btn btn-primary">Simpan</button>
            <a href="<?=url('kelasjabatan')?>" class="btn btn-warning">Kembali</a>
          </div>
        </form>
      </div>
    </div>
  </div>
</section>
<?php } else { ?>
  <section class="content-header">
    <h1>
      Unit Kerja
    </h1>
  </section>
  <!-- Main content -->
  <section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="row">
      <div class="col-md-12">
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">Data Unit Kerja</h3>
          </div>
            <div class="box-body">
              <a href="<?=url($url.'&tambah')?>" class="btn btn-info">Tambah</a>
              <hr>
              <?php
              if(Session::get('info')){
                echo Session::get('info');
                Session::destroy('info');
              }
              ?>
              <table class="table table-bordered dt">
                <thead>
                  <tr>
                    <td width="50px">No</td>
                    <td>Unit Kerja</td>
                    <td width="40px">Edit</td>
                    <td width="40px">Delete</td>
                  </tr>
                </thead>
                <tbody>
                  <?php
                    $i=0;
                    $data=$db->ObjectBuilder()->get($table);
                    foreach ($data as $row) {
                      ?>
                      <tr>
                        <td class="text-center"><?=++$i?></td>
                        <td><?=$row->unit_kerja?></td>
                        <td class="text-center">
                          <a href="<?=url($url.'&ubah&id='.$row->$primaryKey)?>" class="btn btn-warning btn-sm"><i class="fa fa-edit"></i> </a>
                        </td>
                        <td class="text-center">
                          <a href="<?=url($url.'&hapus&id='.$row->$primaryKey)?>" class="btn btn-danger btn-sm" onclick="return confirm('Hapus data?')"><i class="fa fa-trash"></i> </a>
                        </td>
                      </tr>
                      <?php
                    }
                   ?>
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
        </div>
      </div>
    </div>
  </section>
<?php  } ?>
